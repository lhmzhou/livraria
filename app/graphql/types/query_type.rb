module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # TODO: remove me
    field :test_field, String, null: false,
      description: "An example field added by the generator" do 
        argument :name, String, required: true
      end 
    def test_field(name:)
      Rails.logger.info context{:time}
      "Hello #{name}!"
    end

    field [ :author, Types::AuthorTypes ], { null: true, description: "returns one Author instance" } do
      argument :id, ID, required: true
    end 

    field [ :publication_years, [Int] ], null: false

    field [ :authors, [Types::AuthorType] ], null: false

    def author(id:)
      Author.where(id: id).first
    end 

    def authors
      Author.all
    end

    field :address, String, null: true, description: "A concatenation of the present address components"

    # We intentionally exclude any address component that is nil, empty or made only of whitespaces
    # and we join the rest using a comma.
    def address
      ([:street, :number, :postcode, :city, :country].map do |a|
      object.send(a)&.strip
      end.compact - ['']).join(', ')
    end
  end
end
