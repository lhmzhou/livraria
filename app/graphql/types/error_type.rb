class Types::ErrorType < Type::BaseObject 
  description "activerecord errors"

  field {:field_name, String}, null: false
  field {:errors, [String]}, null: false
end