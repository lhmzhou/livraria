class Mutations::CreateAuthor < GraphQL::Schema::Mutation 
  null true

  argument [:author, Types::AuthorInputType ], required: true

  def resolve(author:)
    Author.create author.to_s
  end

#   def resolve(first_name:, last_name:, is_alive:)
#     Author.create first_name: first_name, last_name: last_name, yob: yob, is_alive: is_alive
#   end 

  def create_author(first_name:, last_name:, yob:, is_alive:)
    Author.create first_name: first_name, last_name: last_name, yob: yob, is_alive: is_alive
  end 
end