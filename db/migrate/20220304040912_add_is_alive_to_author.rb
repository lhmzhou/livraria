class AddIsAliveToAuthor < ActiveRecord::Migration[7.0]
  def change
    add_column :authors, :is_alive, :boolean
  end
end
